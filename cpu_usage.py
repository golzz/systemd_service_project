#!/usr/bin/python3
import psutil, sys, os, time, math, json


def get_memory_usage():
    used_memory = psutil.virtual_memory().total - psutil.virtual_memory().available
    return used_memory


print("Memory usage is {} GB".format(int(get_memory_usage() / (1024 * 1024 * 1024))))
print("Memory usage is {} MB".format(int(get_memory_usage() / (1024 * 1024))))
print("Memory usage is {} B".format(int(get_memory_usage())))


def running_processes_highest_usage():
    list_processes = []
    for p in psutil.process_iter():
        try:
            p_info = p.as_dict(attrs=['pid', 'name'])
            p_info['vms'] = p.memory_info().vms
            list_processes.append(p_info)
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass

    list_processes = sorted(list_processes, key=lambda p_obj: p_obj['vms'], reverse=True)
    return list_processes


def beep_sound():
    os.system('say "Beep Beep Beep"')
    # sys.stdout.write('\a')
    # sys.stdout.flush()


# get_memory_usage()
# running_processes_highest_usage()
#
# x = running_processes_highest_usage()
# for i in x[:5]:
#     print("Processes: ",i)


mem_limit, x, minutes = sys.argv[1:]


def bytes_conversion(mem_limit):
    if x == 'MB':
        mem_limit = int(mem_limit) * (1024 ** 2)
        return mem_limit
    elif x == 'GB':
        mem_limit = int(mem_limit) * (1024 ** 3)
        return mem_limit


mem_limit = bytes_conversion(mem_limit)
# print("memLimitB: ",mem_limit)
while True:
    time.sleep(int(minutes) * 60)
    used_mem = get_memory_usage()
    if used_mem >= int(mem_limit):
        print("List of Running Processes:")
        processes = running_processes_highest_usage()
        # beep_sound()
        with open("/tmp/test.txt", "w") as f:
            for el in processes[:15]:
                print(el)
                print(" ".join([str(s) for s in el.items()]), file=f)
        print("UsedMem: ", used_mem)
        print("MemLimit: ", mem_limit)

    else:
        print("No Harm")

