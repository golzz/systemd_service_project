#  💻 Assignment03 - Systemd Service 💻

Link to Repository: https://bitbucket.org/golzz/systemd_service_project/src/master/
 
 
- - -
### 👉 **Table Of Contents**

* [Introduction](#markdown-header-introduction)

* [Installation](#markdown-header-installation)
	
* [Specification](#markdown-header-specification)



- - -


![N|Solid](https://community-cdn-digitalocean-com.global.ssl.fastly.net/variants/KKmMWfLVAfvfUsQWr1svWfys/035575f2985fe451d86e717d73691e533a1a00545d7230900ed786341dc3c882)


- - -

# Introduction

**Systemd** is a collection of system management daemons, utilities, and libraries which serves as a replacement of System V init daemon. **Systemd** functions as central 
management and configuration platform for UNIX like system **Systemctl** is a systemd utility that is responsible for Controlling the systemd system and service manager.
This project create a service in systemd by running a python script at a specific interval. The service should give a warning and a status report when the system has reached a
certain RAM usage.


- - -



# Installation


``
$ git clone https://bitbucket.org/golzz/systemd_service_project.git
``

Run this command to make the bash file executable: 

``
$ sudo chmod +x /home/[user]/[project Folder]/file.sh
``

For execute:

``
$ ./file.sh
``

* By executing the bash file **ram_usage.service** and **.argsconf** files will move from project directory to destination directory.

* service: /etc/systemd/system

* .argsconf: /etc



Reload all unit files, and recreate entire dependency tree

``
$ sudo systemctl daemon-reload 
``

To start a service

``
$ sudo systemctl start ram_usage.service
``

Use this command to verify a service is active or not

``
$ sudo systemctl status ram_usage.service
``

To stop an active service use the following command

``
$ sudo systemctl stop ram_usage.service
``

- - -

# Specification


Three arguments are required for executing the program.

**.argconf** is created to pass arguments to service and added to **EnvironmentFile** in systemd Service definition. Arguments value can be changed by user/admin.

In case of warning, program is going to create a txt file in **/tmp/test.txt** with top 15 processes with highest usage.







---

